# 介绍

嘿，欢迎来到[SourceHot](https://github.com/SourceHot)开源组织，主要以源代码注释分析为主要开发方向。

英文文档请看 [README](README.md) .

## 链接
- GitHub : https://github.com/SourceHot


## 发起人

- [HuiFer](https://github.com/huifer)
  - Email: huifer97@163.com
  - GitHub: https://github.com/huifer



## 项目

| # | 项目名称 | 项目描述 |
|---|---|---|
| 1    | [Gitee](https://gitee.com/source-hot/spring-framework-read)、[GitHub](https://github.com/SourceHot/spring-framework-read) | Spring Framework 源码阅读仓库       |
| 2    | [Gitee](https://gitee.com/source-hot/spring-boot)、[GitHub](https://github.com/SourceHot/spring-boot) | Spring Boot 源码阅读仓库            |
| 3    | [Gitee](https://gitee.com/source-hot/spring-cloud-consul)、[GitHub](https://github.com/SourceHot/spring-cloud-consul) | Spring Cloud Consul 源码阅读仓库    |
| 4    | [Gitee](https://gitee.com/source-hot/spring-cloud-config)、[GitHub](https://github.com/SourceHot/spring-cloud-config) | Spring Cloud Config 源码阅读仓库    |
| 5    | [Gitee](https://gitee.com/source-hot/feign)、[GitHub](https://github.com/SourceHot/feign) | Feing源码阅读仓库                   |
| 6    | [Gitee](https://gitee.com/source-hot/spring-cloud-openfeign)、[GitHub](https://github.com/SourceHot/spring-cloud-openfeign) | Spring Cloud OpenFeign 源码阅读仓库 |
| 7    | [Gitee](https://gitee.com/source-hot/mybatis-3)、[GitHub](https://github.com/SourceHot/mybatis-3) | Mybatis源码阅读仓库                 |

## 如何加入

- 在[此项目](https://gitee.com/source-hot/sourcehot.github.io)的 [issues](https://gitee.com/source-hot/sourcehot.github.io/issues/I4PFRV) 提交申请需求
- 发送邮件到 [huifer97@163.com](huifer97@163.com) 并注明你的GitHub ID, 如 [pychfarm_admin](https://gitee.com/pychfarm_admin).

## 操作说明

- 英文文档 : [OperationInstructions](OperationInstructions.md)

- 中文文档: [操作指南](OperationInstructions_CN.md)

